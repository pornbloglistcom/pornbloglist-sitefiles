# PornBlogList.com Site Files

These files are available publicly so that user's who wish to have their blogs included in the site's listing.

## Contributing

### Prerequisites

Ensure that the following code is added to the head of your blog's template.

```
#!html
<meta name="pornbloglist.com" content="" />
```

This is used to ensure the site is still around and hasn't gone offline cluttering up the list.

### Supported Domains

We currently only support the following domains.

- blogspot.com
- tumblr.com

Additional ones will be added in the future.  If you'd like to see one specifically
please open an issue.

### Required information

The following information is required to add your site to the listing.

* name
* url
* category

Additionally you can include a description field.  This isn't currently used
but will likely be in the future.

For more information look at the files currently in the repo.

### Contribute Files Directly

1. Fork the repository.
2. Create a branch.
3. Add your new file following the example of the files already in the repo.
4. Commit and push your branch.
5. Open a pull request.

A good guide on this can be found at [blog.scottlowe.org](http://blog.scottlowe.org/2015/01/27/using-fork-branch-git-workflow/).

### Open An Issue

1. Head on over to the [issues page](https://bitbucket.org/pornbloglistcom/pornbloglist-sitefiles/issues) and hit the "Create issue" button.
2. Include the following information.

### Follow Up

Once the site has been accepted it may take up to 24 hours for it to appear on the site.
The files are pulled from the master branch once a day.

### What Will Prevent Me From Being Able To Add My Site?

I'll be looking for shady methods of advertising primarily.  Though if I refuse your site
for any reason I'll notify you either in the issue you've opened or in your pull request.
You'll be free to fix if you want and resubmit.
